const reverse = require('./reverse');

describe('When calling reverse function', () => {
    it('should be function', () => {
        expect(typeof reverse).toEqual('function');
    });

    it('should return empty string if no argument passed', () => {
        expect(reverse()).toEqual('');
    });

    it('should return empty string if argument is not string', () => {
        expect(reverse(123)).toEqual('');
    });

    it('should return jeskela if aleksej is argument', () => {
        expect(reverse('aleksej')).toEqual('jeskela');
    });
});