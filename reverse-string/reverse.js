reverse = (stringToReverse) => {
    if (typeof stringToReverse !== 'string') {
        return '';
    }

    // const chars = stringToReverse.split('');
    // let reversedString = '';

    // for (let i = stringToReverse.length - 1; i >= 0; --i) {
    //     reversedString += chars[i];
    // }

    // return reversedString;

    return [...stringToReverse].reverse().join('');
}

module.exports = reverse;