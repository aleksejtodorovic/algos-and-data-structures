const steps = require('./steps');

describe('When I call steps function', () => {
    it ('should be defined', () => {
        expect(typeof steps).toEqual('function');
    });

    it ('should return empty string if no argument passed', () => {
        expect(steps()).toEqual('');
    });

    it ('should return empty string if argument is not number', () => {
        expect(steps('5')).toEqual('');
    });

    describe('Arguments are valid', () => {
        beforeEach(() => {
            jest.spyOn(console, 'log');
        });

        afterEach(() => {
            console.log.mockRestore();
        });

        it ('should return correct value when passing 1 as argument', () => {
            steps(1);

            expect(console.log.mock.calls[0][0]).toEqual('#');
        });

        it ('should return correct value when passing 4 as argument', () => {
            steps(4);

            expect(console.log.mock.calls[0][0]).toEqual('#   ');
            expect(console.log.mock.calls[1][0]).toEqual('##  ');
            expect(console.log.mock.calls[2][0]).toEqual('### ');
            expect(console.log.mock.calls[3][0]).toEqual('####');
        });
    });
});