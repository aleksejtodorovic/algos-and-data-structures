function steps(number) {
    if (typeof number !== 'number') {
        return '';
    }

    for (let row = 0; row < number; ++row) {
        let reached = false,
            outputString = '';

        for (let position = 0; position < number; ++position) {
            if (reached || position > row) {
                reached = true;
            }

            outputString += reached ? ' ' : '#';
        }

        console.log(outputString);
    }
}

module.exports = steps;