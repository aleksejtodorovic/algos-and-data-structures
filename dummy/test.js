const test = require('./index.js');

describe('Tests of test method', () => {
    it('should have test method defined', () => {
        expect(typeof test).toEqual('function');
    });

    it('should return 1 if argument is 1', () => {
        expect(test(1)).toEqual(1);
    });

    it('should return 2 if argument is 2', () => {
        expect(test(2)).toEqual(2);
    });
});