chunk = (array, chunkLength) => {
    const chunked = [],
        arrayLength = array.length;

    for(let i = 0, tmp = []; i < arrayLength; ++i) {
        tmp.push(array.shift());

        if (tmp.length % chunkLength === 0 || i === (arrayLength - 1)) {
            chunked.push(tmp);
            tmp = [];
        }
    }

    return chunked;
}

module.exports = chunk;