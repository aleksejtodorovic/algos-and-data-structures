const reverseInt = require('./index');

describe('When reverseInt is called', () => {
    it('should be function', () => {
        expect(typeof reverseInt).toEqual('function');
    });

    it('should return NaN if no arguments passed', () => {
        expect(reverseInt()).toEqual(NaN);
    });

    it('should return 321 if 123 argument passed', () => {
        expect(reverseInt(123)).toEqual(321);
    });

    it('should return -321 if -123 argument passed', () => {
        expect(reverseInt(-123)).toEqual(-321);
    });

    it('should return 5 if 500 argument passed', () => {
        expect(reverseInt(500)).toEqual(5);
    });

    it('should rteturn 0 if 0 argument passed', () => {
        expect(reverseInt(0)).toEqual(0);
    });
});