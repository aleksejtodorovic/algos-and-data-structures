reverseInt = (numb) => {
    if (!Number.isInteger(numb)) {
        return NaN;
    }

    return parseInt(numb.toString().split('').reverse().join(''), 10) * Math.sign(numb);
}

module.exports = reverseInt;