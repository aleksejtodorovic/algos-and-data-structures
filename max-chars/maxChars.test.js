const reverse = require('./maxChars');

describe('When calling maxChars function', () => {
    it('should be function', () => {
        expect(typeof maxChars).toEqual('function');
    });

    it('should return empty string if no argument passed', () => {
        expect(maxChars()).toEqual('');
    });

    it('should return empty string if argument is not string', () => {
        expect(maxChars(123)).toEqual('');
    });

    it('should return jeskela if aleksej is argument', () => {
        expect(maxChars('test')).toEqual('t');
    });
});