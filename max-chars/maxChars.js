maxChars = text => {
    if (typeof text !== 'string') {
        return '';
    }

    const charMap = {};

    for(let char of text) {
        charMap[char] = charMap[char] + 1 || 1;
    }

    return Object.keys(charMap).reduce((charKey, max) => {
        if(charMap[charKey] > charMap[max]) {
            max = charKey;
        }

        return max;
    }, 0);
}

module.exports = maxChars;