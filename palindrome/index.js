isPalindrome = (str) => {
    if (typeof str !== 'string') {
        return false;
    }

    // FIRST SOLUTION
    // const chars = [...str];
    // let result = true;

    // for(let maxIndex = chars.length, i = 0; i < maxIndex / 2; ++i) {
    //     if (chars[i] !== chars[maxIndex - i - 1]) {
    //         result = false;
    //         break;
    //     }
    // }

    // return result;

    //SECOND SOLUTION
    const reversedString = str.split('').reverse().join('');

    return reversedString === str;
}

module.exports = isPalindrome;