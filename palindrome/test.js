const isPalindrome = require('./index');

describe('When isPalindrome is called', () => {
    it('should be function', () => {
        expect(typeof isPalindrome).toEqual('function');
    });

    it('should return false if no arguments passed', () => {
        expect(isPalindrome()).toEqual(false);
    });

    it('should return false if "notPalindrome" argument passed', () => {
        expect(isPalindrome('notPalindrome')).toEqual(false);
    });

    it('should return true if "goog" argument passed', () => {
        expect(isPalindrome('goog')).toEqual(true);
    });

    it('should return true if "anavolimilovana" argument passed', () => {
        expect(isPalindrome('anavolimilovana')).toEqual(true);
    });
});